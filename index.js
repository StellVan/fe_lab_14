const http = require('http');
const fs = require('fs');
const url = require('url');

module.exports = () => {
  const server = http.createServer((req, res) => {
    if (!fs.existsSync('./files')) {
      fs.mkdirSync('./files');
    }

    let writeToLogs = function (message, end, type) {
      fs.readFile('./log.json', (err, data) => {
        if (err) {
          res.writeHead(400, { 'Content-type': 'text/html' });
          res.end('Error!');
        } else if (data) {
          let body = JSON.parse(data);
          body.logs.push({
            message: message,
            time: new Date().getTime(),
          });

          fs.writeFile('./log.json', JSON.stringify(body), 'UTF-8', (err) => {
            if (err) {
              res.writeHead(400, { 'Content-type': 'text/html' });
              res.end('Error!');
            } else {
              res.writeHead(200, { 'Content-type': type });
              res.end(end);
            }
          });
        }
      });
    };

    let requestUrl = url.parse(req.url, true);

    if (req.method === 'POST' && requestUrl.pathname === '/file') {
      let { filename, content } = requestUrl.query;

      if (!filename || !content) {
        res.writeHead(400, { 'Content-type': 'text/html' });
        res.end('Input values are invalid!');
      } else {
        fs.writeFile(`./files/${filename}`, content, 'UTF-8', (err) => {
          if (err) {
            res.writeHead(400, { 'Content-type': 'text/html' });
            res.end('Error!');
          } else {
            writeToLogs(
              `New file with name ${filename} saved`,
              'Posted!',
              'text/html'
            );
          }
        });
      }
    }

    if (req.method === 'GET') {
      if (requestUrl.pathname === '/logs') {
        if (!requestUrl.query.from) {
          fs.readFile('./log.json', (err, data) => {
            if (err) {
              res.writeHead(400, { 'Content-type': 'text/html' });
              res.end('No such logs!');
            } else if (data) {
              writeToLogs(`Logs were checked`, data, 'application/json');
            }
          });
        } else if (requestUrl.query.from) {
          let { from, to } = requestUrl.query;

          fs.readFile('./log.json', (err, data) => {
            if (err) {
              res.writeHead(400, { 'Content-type': 'text/html' });
              res.end('No such logs!');
            } else if (data) {
              let body = JSON.parse(data).logs.filter(
                (el) => el.time > from && el.time < to
              );

              writeToLogs(
                `Logs were checked`,
                JSON.stringify({ logs: body }),
                'application/json'
              );
            }
          });
        }
      } else if (requestUrl.pathname.startsWith('/file')) {
        let fileName = requestUrl.path.split('/')[2];

        fs.readFile(`./files/${fileName}`, (err, data) => {
          if (err) {
            res.writeHead(400, { 'Content-type': 'text/html' });
            res.end('No such file!');
          } else if (data) {
            writeToLogs(
              `File with name ${fileName} was opened`,
              data,
              'text/html'
            );
          }
        });
      }
    }
  });

  server.listen(process.env.PORT || 8080);
};
